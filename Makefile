all: index.html

clean:
	rm -f index.html index.md
	rm -rf cache figure

clean_ghp:
	rm -f index.md index.Rmd
	rm -f pml-testing.csv pml-training.csv
	rm -rf cache

index.html:
	R -e "library(knitr); knit2html('index.Rmd')"
