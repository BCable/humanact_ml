```{r,echo=FALSE}
start_time <- Sys.time()
start_time <- as.numeric(start_time)
```

Human Activity Recognition Machine Learning
===========================================

Author: Brad Cable

Illinois State University

For Practical Machine Learning - Johns Hopkins University (Coursera)

## Data Description ##

The data being analyzed was previously researched by Wallace Ugulino, Eduardo
Velloso, and Hugo Fuks in the paper and data available here:

http://groupware.les.inf.puc-rio.br/har

The main part of their research and purpose of machine learning in their context
is the following:

> This human activity recognition research has traditionally focused on
> discriminating between different activities, i.e. to predict "which" activity
> was performed at a specific point in time (like with the Daily Living
> Activities dataset above). The approach we propose for the Weight Lifting
> Exercises dataset is to investigate "how (well)" an activity was performed by
> the wearer. The "how (well)" investigation has only received little attention
> so far, even though it potentially provides useful information for a large
> variety of applications,such as sports training.

This paper attempts to create a classifier that performs similar analysis and
classifies by how well an individual performs actions and not which actions the
individual is performing.

## Preparation ##

### Loading Libraries ###

```{r}
library(caret)
library(randomForest)
library(rpart)
library(ggplot2)
```

### Loading Data ###

```{r}
training <- read.csv("pml-training.csv")
testing <- read.csv("pml-testing.csv")
```

### Setting Seed for Reproducibility ###

```{r}
set.seed(43121)
```

## Data Cleaning and Variable Analysis ##

### Proportion of Missing Values ###

This function calculates out what percentage of the data has missing values or not.

```{r}
propMissing <- function(df, name){
	vect <- df[[name]]
	sum((is.na(vect) | vect == "")*1)/length(vect)
}
```

Calculating out the percentage of missing values for each field:

```{r}
fieldsMissing <- sapply(names(training), function(name){ propMissing(training, name)})
```

Plotting the proportion of missing values, you can see that all variables only have two levels of missing data.

```{r}
plot(fieldsMissing, ylab="Proportion Missing")
```

Looking at the levels:

```{r}
lvls <- levels(factor(fieldsMissing))
lvls
```

Either a particular variable has one hundred percent of the values present, or it has a missing value proportion of: `r round(as.numeric(lvls[2]), 4)`, which is quite high.

My hypothesis is that these fields with missing values are all the same records and the same values missing in each case, which I will test next.

### Testing Hypothesis About Missing Values ###

Now to test the hypothesis that all the values that are missing are the same records.

First we need to subset the original training set and view only the fields that are theorized to be missing under the same circumstances:

```{r}
whichFieldsMissing <- fieldsMissing
names(whichFieldsMissing) <- NULL
whichFieldsMissing <- whichFieldsMissing != 0
sketchyTrainingFields <- training[whichFieldsMissing]
```

Now we need to calculate per column which records are present.

```{r}
simpleSketchyTrainingFields <- sapply(names(sketchyTrainingFields), function(name){
	is.na(sketchyTrainingFields[[name]]) |
	sketchyTrainingFields[[name]] == ""
})
```

And loop through to see if they are all equal or not:

```{r}
allSame <- TRUE
for (i in 2:ncol(simpleSketchyTrainingFields)){
	if(sum((
		simpleSketchyTrainingFields[,1] !=
		simpleSketchyTrainingFields[,i]
	)*1) > 0){
		allSame <- FALSE
		break
	}
}
allSame
```

The result is that they are all the same records with missing values.

### Converting to Presence ###

When the model trains it will likely with such a low quantity of actual data be only training on the *presence* of these values, and not the values themselves, so it might be important to provide the presence of these values to help train but not to provide the values themselves as they could produce strange results.

To do this, we can take any of the fields that have this missing values pattern and convert those fields into a single field that is a boolean value on the presense of data in those fields.  This provides the ability to learn on the presense without skewing the data to these values that are infrequent enough to train on.

First we need to remove those fields from the training and test sets.

```{r}
cleanTraining <- training[!whichFieldsMissing]
```

Now we can add a new field that is the presence of these fields or not:

```{r}
cleanTraining$presence <- !is.na(training$max_roll_belt)
```

### Timestamps ###

Taking a look at the timestamps provided there are some strange aspects about them.  The fields in question are "raw_timestamp_part_1", "raw_timestamp_part_2", and "cvtd_timestamp".

#### cvtd_timestamp ####

Looking at this variable you can see that there is very little variability and that each of the rows can be grouped into a small amount of timeframes.

```{r}
lvls <- levels(training$cvtd_timestamp)
lvls
```

With only `r length(lvls)` levels detailing down to the minute, you would expect a lot more different types of results in this area.

#### raw_timestamp_part_1 ####

```{r}
lvls <- length(levels(factor(training$raw_timestamp_part_1)))
lvls
```

This field has a bit more variability to it with `r lvls` levels, but the format appears to have a strange property to it.  Namely, the first half of the values appear to be in some way related to the cvtd_timestamp value and the second half appears to be a count.  It would be difficult to display and analyze all `r length(lvls)` values here and reverse engineering this field is not desired when there are even stranger aspects of the rest of these fields.

#### raw_timestamp_part_2 ####

```{r}
lvls <- length(levels(factor(training$raw_timestamp_part_2)))
lvls
```

With `r lvls` levels, this can be seen as having too high of variability to predict anything of significance.  On top of which, if the value is microseconds, this value can do little to predict how well the individual is doing at the action since the information would essentially be random noise.

#### What to do? ####

Because of the time information being essentially either too low of precision data or too high of precision data, all time data is going to be stripped out of the model.

```{r}
cleanTraining$cvtd_timestamp <- NULL
cleanTraining$raw_timestamp_part_1 <- NULL
cleanTraining$raw_timestamp_part_2 <- NULL
```

### X ###

X appears to be a simple row count, which does us no good since this isn't exactly timeseries data.  The timestamps exist but the order of them are not particularly relevant and the values of them are not particularly relevant, so we'll strip this value out.

```{r}
head(cleanTraining$X, 100)
```

```{r}
cleanTraining$X <- NULL
```

### Variable Importance of Exploratory Random Forest ###

By calculating the variable importance of a small subset of the data using a random forest, we can gather more information about what variables are useful and what variables are not.

To start, we subset the training data into 1% of the original for a quick analysis of the data.

```{r}
quickPartition <- createDataPartition(cleanTraining$classe, p=0.01, list=FALSE)
```

Now we generate a random forest model on the smaller dataset:

```{r,cache=TRUE}
subsetTest <- cleanTraining[quickPartition,]
subsetModelRF <- train(classe ~ ., data=subsetTest, method="rf")
```

Calculating variable importance, cleaning it up, and graphing the results:

```{r}
importance_graph <- function(model){
	subsetImportance <- varImp(model)
	importanceGraphData <- subsetImportance$importance
	importanceGraphData$Name <- row.names(importanceGraphData)
	importanceGraphData <- importanceGraphData[order(-importanceGraphData[[1]]),]
	importanceGraphData$Name <- factor(importanceGraphData$Name, levels=importanceGraphData$Name)
	importanceGraphData <- importanceGraphData[importanceGraphData$Overall > 0,]

	g <- ggplot(data.frame(x=importanceGraphData$Overall, y=importanceGraphData$Name), aes(y=y, x=x))
	g + geom_point() + xlab("Overall Importance Factor") + ylab("Name")
}
```

```{r,fig.width=6,fig.height=12}
importance_graph(subsetModelRF)
```

From this analysis we can see that the user, specificially who is exercising, has very little predictive value.  Also to note is that the new_window variable has very little predictive value as is our calculation of presence in regards to the missing data.  This should have been expected based on the proportion of missing values to actual information, so the hypothesis that the presence could have affected a prediction model is false.

These variables with an importance factor that have a significant drop and are below the value 1 can also be stripped out of the training data.

```{r}
cleanTraining$new_window <- NULL
cleanTraining$user_name <- NULL
cleanTraining$presence <- NULL
```

## CART Exploratory Model ##

While a Random Forest worked, it would be interesting to see how a CART model worked on this data, so using the same subset dataset we can take a quick look at the data before using up a large quantity of the test data for the final models.

Keep in mind since we are re-using the subsetTest variable, this is modeling with the user_name, new_window, and presence variables in mind, still, but this should not affect the overall exploratory model in the long run.

```{r,cache=TRUE}
subsetModelCART <- train(classe ~ ., data=subsetTest, method="rpart")
```

The CART model itself looks like the following, with noticeably less variables and not very many leaves put into the model.  For CART based classification trees, this is actually a fairly good sign as too many variables could produce large amounts of overfitting:

```{r}
subsetModelCART$finalModel
```

An importance graph, as used above, of the same data:

```{r,fig.width=6,fig.height=6}
importance_graph(subsetModelCART)
```

## Creating Test Models ##

### Structure of Final Clean Testing Data ###

Taking a look at the final clean testing data to verify accuracy and to just in general realize what we are using:

```{r}
str(cleanTraining)
```

### K-Folds ###

We will be using K-Folds (6 folds) to generate testing data and validation data from the original testing dataset to generate accuracy predictions before touching the final testing data provided.

```{r,cache=TRUE}
finalModelFolds <- createFolds(cleanTraining$classe, k=6, list=FALSE)
```

### Random Forest ###

#### Data ####

```{r}
rfTrainData <- cleanTraining[finalModelFolds==1,]
rfPredictData <- cleanTraining[finalModelFolds==2,]
```

#### Building Model ####

```{r,cache=TRUE}
rfModel <- train(classe ~ ., data=rfTrainData, method="rf")
```

#### Generating Predictions ####

```{r}
rfPredictions <- predict(rfModel, newdata=rfPredictData)
```

#### Accuracy Checks ####

```{r}
rfConfusion <- confusionMatrix(rfPredictions, reference=rfPredictData$classe)
rfConfusion
```

### CART ###

#### Data ####

```{r}
cartTrainData <- cleanTraining[finalModelFolds==3,]
cartPredictData <- cleanTraining[finalModelFolds==4,]
```

#### Building Model ####

```{r,cache=TRUE}
cartModel <- train(classe ~ ., data=cartTrainData, method="rpart")
```

#### Generating Predictions ####

```{r}
cartPredictions <- predict(cartModel, newdata=cartPredictData)
```

#### Accuracy Checks ####

```{r}
cartConfusion <- confusionMatrix(cartPredictions, reference=cartPredictData$classe)
cartConfusion
```

## Final Predictions and Analysis ##

Decided to use the random forest, an accuracy rate of `r round(rfConfusion$overall[[1]], 4)` is much better than the CART model which has an accuracy rate of `r round(cartConfusion$overall[[1]], 4)`.  Now time to predict on the testing data which hasn't been touched yet.

### Final Random Forest ###

#### Building Model ####

```{r,cache=TRUE}
finalRFModel <- train(classe ~ ., data=cleanTraining, method="rf")
```

#### Generating Predictions ####

```{r}
finalPredictions <- predict(finalRFModel, newdata=testing)
finalPredictions
```

## Coursera Submission ##

Predictions submitted to Coursera with 20/20 resulting in correct values.

## Hardware and OS Information ##

```{r,echo=FALSE}
proc_info <- system('grep -m 1 "model name" /proc/cpuinfo | sed "s/^.*: //g"', intern=TRUE)
proc_count <- system('grep processor /proc/cpuinfo | wc -l', intern=TRUE)
mem_info <- round(as.numeric(system('grep MemTotal /proc/meminfo | sed "s/[^0-9]//g"', intern=TRUE))/1024/1024)
os_info <- system('uname -a', intern=TRUE)
r_info <- version$version.string
caret_info <- packageVersion("caret")
rf_info <- packageVersion("randomForest")
rpart_info <- packageVersion("rpart")
ggplot2_info <- packageVersion("ggplot2")
knitr_info <- packageVersion("knitr")
```

|`r paste(rep("&nbsp;",42), collapse="")`| |
|:---|:---|
|Memory:|`r mem_info`GB RAM|
|Processor:|`r proc_info`|
|Processor Core Count:|`r proc_count` Logical Threads|
|OS:|`r system('uname -sr', intern=TRUE)`|
|R version:|`r r_info`|
|R Packages Versions:|caret `r caret_info`|
||randomForest `r rf_info`|
||rpart `r rpart_info`|
||ggplot2 `r ggplot2_info`|
||knitr `r knitr_info`|

## Report Generation ##

```{r,echo=FALSE}
end_time <- Sys.time()
end_time <- as.numeric(end_time)
duration_min <- (end_time-start_time)/60
```

Report was generated using R Markdown and knitr in `r duration_min` minutes at `r format(Sys.time(), "%c")`

## Citation/Reference ##

The data used is available here: http://groupware.les.inf.puc-rio.br/har

Velloso, E.; Bulling, A.; Gellersen, H.; Ugulino, W.; Fuks, H. Qualitative Activity Recognition of Weight Lifting Exercises. Proceedings of 4th International Conference in Cooperation with SIGCHI (Augmented Human '13) . Stuttgart, Germany: ACM SIGCHI, 2013.
